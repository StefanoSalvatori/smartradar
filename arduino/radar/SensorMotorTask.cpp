#include "SensorMotorTask.h"
#include "MsgService.h"
#include "MsgType.h"
#include "config.h"

#define SERVO_STEP 1
#define SERVO_START_ANGLE 90
#define COVERAGE_ANGLE 180 

#define DIST_MAX 30
#define DIST_MIN 3
#define DIST_SPIKE 50

SensorMotorTask::SensorMotorTask() {
  this->proxSensor = new NewPing(TRIG_PIN, ECHO_PIN, DIST_MAX);
  this->servo = new Servo();
  this->servoStep = SERVO_STEP;
  this->led = new Led(CONN_LED_PIN);
  this->state = IDLE;
}

void SensorMotorTask::tick() {
  const String& msg = MsgService.isMsgAvailable() ? MsgService.receiveMsg()->getContent() : "?";
  if (msg == OFF) {
    led->switchOff();
    this->servoStep = this->servo->read()  > SERVO_START_ANGLE ? -SERVO_STEP : SERVO_STEP; 
    state = RELOCATION;
  } else {
    int currAngle;
    unsigned int distance;
    switch (state) {
      case IDLE :
        if (msg == ON) {
          this->servo->attach(SERVO_PIN);   
          state = SETTING;
        }
        break;
      case SETTING:
        for (int i = this->servo->read(); i >= 0; i -= SERVO_STEP) {
          servo->write(i);
          delay(30);
        }
        led->switchOn();
        state = SCANNING;
        break;
      case SCANNING :     
        currAngle = this->servo->read();
        if (currAngle >= COVERAGE_ANGLE || currAngle <= 0) {
          this->servoStep = -this->servoStep;
        }
        servo->write(currAngle + this->servoStep);
        distance  = computeDistance(); 
        MsgService.send(INFO(distance, currAngle));
        if (msg == TRACK) {
          state = STOP;
        }
        break;
      case STOP :
        distance = computeDistance();
        MsgService.send(INFO(distance, this->servo->read()));
        if (msg == SCAN) {
          state = SCANNING;
        }
        break;
      case RELOCATION: //Relocate the servo at SERVO_START_ANGLE
        int pos = this->servo->read();
        while (pos != SERVO_START_ANGLE) {
          pos += this->servoStep;;
          this->servo->write(pos);
          delay(30);
        }
        this->servo->detach();
        state = IDLE;
    }
  }
}


unsigned int SensorMotorTask::computeDistance() {
  unsigned int dist = this->proxSensor->ping_cm();
  const int diff = dist - prevDist;
  if (abs(diff) > DIST_SPIKE) {
    dist =  NewPing::convert_cm(this->proxSensor->ping_median(2));
  }
  if (dist > DIST_MAX || dist == 0) {
    return DIST_MAX;
  } else if (dist < DIST_MIN) {
    return DIST_MIN;
  }
  prevDist = dist;
  return dist;
}
