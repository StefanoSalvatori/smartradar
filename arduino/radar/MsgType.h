#ifndef __MSG_TYPE__
#define __MSG_TYPE__

/**
 * Receive 
 */
#define ON "ON"
#define OFF "OFF"
#define SCAN "SCAN"
#define TRACK "TRACK"


/**
 * Send
 */
#define INFO(d,a) "INFO " + String(d) + " " + String(a)

#endif
