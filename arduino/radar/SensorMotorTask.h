#ifndef __SENSOR_MOTOR_TASK__
#define __SENSOR_MOTOR_TASK__

#include "Task.h"
#include "NewPing.h"
#include "Servo.h"
#include "Led.h"
#include "Potentiometer.h"

class SensorMotorTask : public  Task {

  public:
    SensorMotorTask();
    void tick();
  private:
    unsigned int computeDistance();

    NewPing* proxSensor;
    Servo* servo;
    Led* led;

    unsigned int prevDist;
    int servoStep;
    enum States {SETTING,IDLE, SCANNING, STOP, RELOCATION} state;
};


#endif
