#ifndef __POTENTIOMETER__
#define __POTENTIOMETER__

#define MAX_VALUE 1023

class Potentiometer { 
public:
  
  Potentiometer(int pin);
  int getValue();
  int getBoundedValue(int min, int max);

protected:
  int pin;  
};

#endif
