#ifndef __TASK__
#define __TASK__

#include "Arduino.h"

class Task {
private:
	int timeElapsed;
	int myPeriod;

public:
	virtual void init(int period) {
		myPeriod = period;
		timeElapsed = 0;
	}

	virtual void tick() = 0;

	bool updateAndCheckTime(int basePeriod) {
		timeElapsed += basePeriod;
		if (timeElapsed >= myPeriod) {
			timeElapsed = 0;
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Retutn the period of this task
	 */
	int getPeriod() {
		return this->myPeriod;
	}

};

#endif
