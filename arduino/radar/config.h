#ifndef __CONFIG__
#define __CONFIG__

#define TRIG_PIN 7
#define ECHO_PIN 8
#define SERVO_PIN 9
#define CONN_LED_PIN 3
#define POT_PIN A0

#endif
