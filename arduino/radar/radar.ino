#include "Arduino.h"
#include "SensorMotorTask.h"
#include "MsgService.h"
#include "Potentiometer.h"
#include "config.h"

#define MIN_VAL 35
#define MAX_VAL 300
#define COVERAGE_ANGLE 180 

SensorMotorTask* sensorMotor;

Potentiometer* pot = new Potentiometer(POT_PIN);


void setup() {

  MsgService.init();

  sensorMotor = new SensorMotorTask();

  //Serial.println(".:Ready:.");
}

void loop() {
  
  sensorMotor->tick();
  
  delay(/*pot->getBoundedValue(MIN_VAL, MAX_VAL)*/35);
}
