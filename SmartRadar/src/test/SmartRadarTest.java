package test;

import smartradar.common.SmartRadar;
import smartradar.comunication.MsgHandler;
import smartradar.comunication.ObservableSerial;
import smartradar.devices.pi4j.PhysicalButton;
import smartradar.devices.pi4j.PhysicalLed;

/**
 * Test.
 *
 */
public final class SmartRadarTest {
    private static final int BAUD_RATE = 9600;
    private static final String SERIAL_PORT = "/dev/ttyS99";

    private SmartRadarTest() {

    }

    /**
     * @param args
     *            args.
     */
    public static void main(final String[] args) {

        final PhysicalLed onLed = new PhysicalLed(Config.LED_ON.pin());
        final PhysicalLed detected = new PhysicalLed(Config.DETECTED_LED.pin());
        final PhysicalLed tracking = new PhysicalLed(Config.TRACKING_LED.pin());
        final PhysicalButton onButton = new PhysicalButton(Config.BUTTON_ON.pin());
        final PhysicalButton offButton = new PhysicalButton(Config.BUTTON_OFF.pin());
        

        ObservableSerial serial;

        try {
            serial = new ObservableSerial(SERIAL_PORT, BAUD_RATE);

            final SmartRadar radar = new SmartRadar();

            MsgHandler.init(serial, radar);
            radar.init(onButton, offButton, onLed, detected, tracking, MsgHandler.getInstance());

            MsgHandler.getInstance().start();
            radar.start();
            
            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
