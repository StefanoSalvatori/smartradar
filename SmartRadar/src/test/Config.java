package test;

/**
 * Configurazione pin del sistema
 *
 */
public enum Config {

    LED_ON(3), DETECTED_LED(4), TRACKING_LED(5), BUTTON_ON(15), BUTTON_OFF(16);

    private final int pin;

    Config(final int p) {
        this.pin = p;
    }

    public int pin() {
        return this.pin;
    }

}
