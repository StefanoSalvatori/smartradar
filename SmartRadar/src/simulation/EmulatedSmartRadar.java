package simulation;

import smartradar.common.AbstractSmartRadar;
import smartradar.common.Logger;
import smartradar.common.ReactiveAgent;
import smartradar.common.event.Msg;
import smartradar.common.event.MsgType;

/**
 * Simula il comportaento del sistema installato sul raspberry
 */
public class EmulatedSmartRadar extends AbstractSmartRadar {

    /**
     * Distanza a cui inizia la fase di tracciamento
     */
    protected static final int MIN_DIST = 50;
    protected static final int RADAR_RANGE = 300;

    private ReactiveAgent arduino;
    private final Controller controller;

    /**
     * @param onButton
     *            bottone per accendere il radar
     * @param offButton
     *            bottone per spegnere il radar
     * @param c
     *            Controller per la comunicazione con la view
     */
    public EmulatedSmartRadar(final EmulatedObservableButton onButton, final EmulatedObservableButton offButton,
            final Controller c) {
        this.controller = c;
        this.onButton = onButton;
        this.offButton = offButton;
        this.offButton.addObserver(this);
        this.onButton.addObserver(this);
        currentState = State.IDLE;

    }

    /**
     * Inizia la comunicazione con arduino.
     * 
     * @param agent
     *            il reactive agent che simula l'arduino
     */
    public void attachComunication(final ReactiveAgent agent) {
        this.arduino = agent;

    }

    /**
     * @return bottone on
     */
    public EmulatedObservableButton getOnButton() {
        return (EmulatedObservableButton) this.onButton;
    }

    /**
     * @return bottone off
     */
    public EmulatedObservableButton getOffButton() {
        return (EmulatedObservableButton) this.offButton;
    }

    /**
     * 
     * @return velocità di flashing dei led
     */
    public int flashSpeed() {
        return FLASH_SPEED;
    }

    protected void objDetected() {
        this.objCount++;
        this.controller.notifyDetected();
    }

    protected void scanComplete() {
        Logger.get().log("#objects found: " + this.objCount);
        this.objCount = 0;
    }

    protected void turnOff() {
        Logger.get().log("Radar Offline");
        this.sendMsgTo(this.arduino, new Msg(MsgType.OFF));
        switchState(State.IDLE);
    }

    protected void turnOn() {
        Logger.get().log("Radar Online");
        this.sendMsgTo(this.arduino, new Msg(MsgType.ON));
        this.switchState(State.CONNECTED);

    }

    protected void stopTracking() {
        controller.notifyTracking(false);
        this.sendMsgTo(this.arduino, new Msg(MsgType.RESUME));
        Logger.get().log("Stop tracking");
        this.switchState(State.CONNECTED);
    }

    protected void startTracking() {
        controller.notifyTracking(true);
        this.prevTrackingDist = 0;
        Logger.get().log("Tracking object dist: " + this.distRecv);
        this.sendMsgTo(this.arduino, new Msg(MsgType.TRACK));
        this.switchState(State.TRACKING);
    }

    protected void parseMsg(final String received) {
        final String[] parsed = received.split(" ");
        this.distRecv = Integer.parseInt(parsed[0]);
        this.angleRecv = Integer.parseInt(parsed[1]);
    }

    @Override
    protected int getMaxDetectDist() {
        return RADAR_RANGE;
    }

    @Override
    protected int getTrackingDist() {
        return MIN_DIST;
    }

    @Override
    protected void trackDistChanged() {
        Logger.get().log("dist: " + this.distRecv);
    }

}
