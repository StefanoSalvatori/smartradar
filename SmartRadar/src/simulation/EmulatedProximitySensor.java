package simulation;

import smartradar.common.Observable;
import smartradar.devices.generic.ProximitySensor;

/**
 * Emula il comportamento di un sensore di prossimità
 *
 */
public class EmulatedProximitySensor extends Observable implements ProximitySensor {
    /**
     * Range massimo del sensore.
     */
    private static final int DETECT_DISTANCE = 300;
    /**
     * Angolo di visuale in gradi
     */
    private static final int SENSOR_VISION_ANGLE = 1;

    private int currentDistance;
    private boolean objDetected;
    private final int trigPin;
    private final int echoPin;

    /**
     * 
     * @param echo
     *            echo pin
     * @param trig
     *            trig pin
     */
    public EmulatedProximitySensor(final int echo, final int trig) {
        this.echoPin = echo;
        this.trigPin = trig;

    }

    public boolean isObjDetected() {
        return this.objDetected;
    }

    @Override
    public int getObjDistance() {
        return this.currentDistance;
    }

    /**
     * 
     * @return trig pin.
     */
    public int getTrigPin() {
        return trigPin;
    }

    /**
     * 
     * @return echo pin
     */
    public int getEchoPin() {
        return echoPin;
    }

    /**
     * 
     * @return l'angolo di visuale  del sensore
     */
    public int getVisionAngle() {
        return SENSOR_VISION_ANGLE;
    }

    /**
     * 
     * @return massima distanza di funzionamento del sensore
     */
    public int getMaxDistance() {
        return DETECT_DISTANCE;
    }

    /**
     * Emula la presenza di un oggetto ad una certa distanza
     * 
     * @param dist
     *            la distanza a cui si trova l'oggetto
     */
    public void changeDistance(final int dist) {
        this.currentDistance = dist >= DETECT_DISTANCE ? DETECT_DISTANCE : dist;
    }
}
