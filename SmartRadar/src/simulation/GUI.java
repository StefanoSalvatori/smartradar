package simulation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Simulazione grafica del radar.
 *
 */
public class GUI extends JFrame {
    private static final long serialVersionUID = -8971511559640473619L;
    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;
    private static final Color DEFAULT_LED_COLOR = Color.LIGHT_GRAY;
    private static final int LED_SIZE = 40;

    private final JButton upSpeed = new JButton(">>");
    private final JButton downSpeed = new JButton("<<");
    private final JLabel speed = new JLabel(" speed:");

    private final JButton onButton = new JButton("ON");
    private final JButton offButton = new JButton("OFF");
    private final JButton onLed = new JButton();

    private final JButton detectedLed = new JButton();
    private final JButton trackingLed = new JButton();
    private final LayoutManager layout = new BorderLayout();
    private final JPanel topPanel = new JPanel(new FlowLayout());
    private final SimulationCanvas canvas = new SimulationCanvas(WIDTH, HEIGHT);
    private final Controller controller;

    /**
     * Costruttore della GUI.
     * 
     * @param controller
     *            Controller
     */
    public GUI(final Controller controller) {
        super("Simulation");
        this.controller = controller;

        this.detectedLed.setPreferredSize(new Dimension(LED_SIZE, LED_SIZE));
        this.trackingLed.setPreferredSize(new Dimension(LED_SIZE, LED_SIZE));
        this.onLed.setPreferredSize(new Dimension(LED_SIZE, LED_SIZE));

        this.onLed.setEnabled(false);
        this.trackingLed.setEnabled(false);
        this.detectedLed.setEnabled(false);
        this.onLed.setBackground(DEFAULT_LED_COLOR);
        this.topPanel.add(onLed);
        this.topPanel.add(onButton);
        this.topPanel.add(offButton);
        this.detectedLed.setBackground(DEFAULT_LED_COLOR);
        this.topPanel.add(detectedLed);
        this.trackingLed.setBackground(DEFAULT_LED_COLOR);
        this.topPanel.add(trackingLed);

        this.topPanel.add(speed);
        this.topPanel.add(downSpeed);
        this.topPanel.add(upSpeed);

        this.onButton.addMouseListener(new MouseAdapter() {
            public void mousePressed(final MouseEvent e) {
                controller.onButtonPressed();
                onLed.setBackground(Color.GREEN);
                canvas.repaint();
            }
        });

        this.offButton.addMouseListener(new MouseAdapter() {
            public void mousePressed(final MouseEvent e) {
                controller.offButtonPressed();
                onLed.setBackground(DEFAULT_LED_COLOR);
                canvas.repaint();
            }
        });

        this.upSpeed.addMouseListener(new MouseAdapter() {
            public void mousePressed(final MouseEvent e) {
                controller.increaseSpeed();
                canvas.repaint();
            }
        });

        this.downSpeed.addMouseListener(new MouseAdapter() {
            public void mousePressed(final MouseEvent e) {
                controller.decreaseSpeed();
                canvas.repaint();
            }
        });

        this.setLayout(layout);
        this.setSize(WIDTH, HEIGHT);
        this.add(topPanel, BorderLayout.PAGE_START);
        this.add(canvas, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent ev) {
                System.exit(-1);
            }
        });

    }

    /**
     * Classe che incapsula la logica della grafica del sistema
     */
    class SimulationCanvas extends JPanel {
        private static final long serialVersionUID = -5338888022808681189L;
        private static final int MIN_OBJ_SIZE = 10;
        private static final int MAX_OBJ_SIZE = 30;
        private static final int SENSOR_WIDTH = 120;
        private static final int SENSOR_HEIGHT = 20;

        private final List<Object> objects = new ArrayList<>();
        private Object click;
        private int angle;

        SimulationCanvas(final int width, final int height) {
            super();
            this.setSize(width, height);
            this.addMouseListener(new MouseAdapter() {
                public void mouseClicked(final MouseEvent e) {
                    final int x = e.getX();
                    final int y = e.getY();
                    if (e.getButton() == MouseEvent.BUTTON1 && click == null) {
                        objects.add(
                                new Object(x, y, new Random().nextInt(MAX_OBJ_SIZE - MIN_OBJ_SIZE + 1) + MIN_OBJ_SIZE));
                    } else if (e.getButton() == MouseEvent.BUTTON3) {
                        objects.remove(objects.stream().filter(o -> o.pointInside(x, y)).findFirst().orElse(null));
                    }
                    repaint();
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        final List<Object> clicked = objects
                                .stream()
                                .filter(o -> o.pointInside(e.getX(), e.getY()))
                                .collect(Collectors.toList());
                        if (clicked.size() != 0) {
                            click = clicked.get(0);
                        }
                    }
                    repaint();
                }

            });
            this.addMouseMotionListener(new MouseMotionListener() {
                @Override
                public void mouseDragged(final MouseEvent e) {
                    if (click != null) {
                        click.setPosition(e.getX(), e.getY());
                        repaint();
                    }
                }

                @Override
                public void mouseMoved(final MouseEvent e) {
                    click = null;
                }
            });
            this.setBackground(Color.WHITE);
        }

        public void paintComponent(final Graphics g) {
            final BufferedImage bufferImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
            final Graphics2D g2 = (Graphics2D) bufferImage.getGraphics();
            g2.setColor(Color.BLACK);
            super.paintComponent(g2);
            objects.forEach(o -> {
                o.draw(g2);
            });
            drawSensor(g2);
            drawVisionCone(g2);
            g.drawImage(bufferImage, 0, 0, null);
            g.dispose();

        }

        public void setRadarAngle(final int anglePos) {
            this.angle = 180 + anglePos;
            this.repaint();
        }

        private void drawSensor(final Graphics g) {
            g.setColor(Color.BLUE);
            g.fillRect(getWidth() / 2 - SENSOR_WIDTH / 2, getHeight() - SENSOR_HEIGHT, SENSOR_WIDTH, SENSOR_HEIGHT);
        }

        private void drawVisionCone(final Graphics g) {
            final int visionAngle = controller.getProxVisionAngle();
            final int range = controller.getRange();
            int detectedDist = range;
            g.setColor(Color.YELLOW);
            final int xC = getWidth() / 2;
            final int yC = getHeight() - SENSOR_HEIGHT;
            double cone = Math.toRadians(this.angle);
            for (int i = 0; i < visionAngle; i++) {
                final int x2 = (int) (xC + range * Math.cos(cone));
                final int y2 = (int) (yC + range * Math.sin(cone));
                g.drawLine(xC, yC, x2, y2);
                cone += Math.toRadians(1);
                for (final Object o : objects) {
                    if (o.intersectLine(xC, yC, x2, y2)) {
                        final int newDist = (int) Point.distance(xC, yC, o.getX(), o.getY());
                        detectedDist = newDist < detectedDist ? newDist : detectedDist;
                    }
                }
            }
            final int dist = detectedDist;
            controller.setDist(dist);
        }
    }

    /**
     * Genercio oggetto quadrato in uno spazio bidimensionale.
     */
    class Object {

        private final Point position;
        private final int size;

        /**
         * @param posX
         *            posizione sull'asse delle x
         * @param posY
         *            posizione sull'asse delle y
         * @param size
         *            grandezza dell'oggetto
         */
        public Object(final int posX, final int posY, final int size) {
            this.position = new Point(posX, posY);
            this.size = size;
        }

        /**
         * 
         * @return coordinata x
         */
        public int getX() {
            return (int) this.position.getX();
        }

        /**
         * 
         * @return coordinata y
         */
        public int getY() {
            return (int) this.position.getY();
        }

        /**
         * 
         * @return la grandezza dell'oggetto
         */
        public int getSize() {
            return this.size;
        }

        /**
         * Disegna un quadrato che rappresenta questo oggetto.
         * 
         * @param g
         *            la grafica nella quale si vuole disegnare
         */
        public void draw(final Graphics g) {
            g.setColor(Color.BLACK);
            g.drawRect(this.getX(), this.getY(), this.size, this.size);
        }

        /**
         * Controlla se l'oggetto interseca una linea.
         * 
         * @param x1
         *            x1 della linea
         * @param y1
         *            y1 della linea
         * @param x2
         *            x2 della linea
         * @param y2
         *            y2 della linea
         * @return true se si intersecano
         */
        public boolean intersectLine(final int x1, final int y1, final int x2, final int y2) {
            return Line2D.linesIntersect(x1, y1, x2, y2, this.getX(), this.getY(), this.getX() + this.size, this.getY())
                    || Line2D.linesIntersect(x1, y1, x2, y2, this.getX(), this.getY(), this.getX(),
                            this.getY() + this.size)
                    || Line2D.linesIntersect(x1, y1, x2, y2, this.getX(), this.getY() + this.size,
                            this.getX() + this.size, this.getY() + this.size)
                    || Line2D.linesIntersect(x1, y1, x2, y2, this.getX() + this.size, this.getY(),
                            this.getX() + this.size, this.getY() + this.size);
        }

        /**
         * Controlle se un punto si trova all'interno dell'oggetto
         * 
         * @param x
         *            x del punto
         * @param y
         *            y del punto
         * @return true se il punto è dentro
         */
        public boolean pointInside(final int x, final int y) {
            return x > this.getX() && x < this.getX() + this.size && y < this.getY() + this.size && y > this.getY();
        }

        /**
         * Cambia la posizione dell'oggetto.
         * 
         * @param x
         *            nuova x
         * @param y
         *            nuova y
         */
        public void setPosition(final int x, final int y) {
            this.position.setLocation(x, y);

        }

    }

    /**
     * Blinka il led 'detected'.
     */
    public void blinkDetectedLed() {
        new Thread(() -> {
            this.detectedLed.setBackground(Color.RED);
            try {
                Thread.sleep(controller.getLedFlashSpeed());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.detectedLed.setBackground(DEFAULT_LED_COLOR);
        }).start();
    }

    /**
     * Accende o spegne il led 'tracking'.
     * 
     * @param b
     *            true se si vuole accendere
     */
    public void setTracking(final boolean b) {
        this.trackingLed.setBackground(b ? Color.YELLOW : DEFAULT_LED_COLOR);
    }

    /**
     * Cambia l'attuale angolo del radar
     * 
     * @param angle
     *            il nuovo angolo
     */
    public void updateAngle(final int angle) {
        this.canvas.setRadarAngle(angle);
    }

}
