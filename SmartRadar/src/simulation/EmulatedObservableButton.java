package simulation;

import smartradar.common.event.ButtonPressed;
import smartradar.devices.generic.Button;
import smartradar.devices.generic.ObservableButton;

/**
 * Classe per emulare il comportamento di un bottone.
 *
 */
public class EmulatedObservableButton extends ObservableButton implements Button {

    private final int pinNum;
    private boolean pressed;

    /**
     * 
     * @param pinNum
     *            il pin in cui sarebbe installato il bottone
     */
    public EmulatedObservableButton(final int pinNum) {
        this.pinNum = pinNum;

    }

    @Override
    public synchronized boolean isPressed() {
        return pressed;
    }

    /**
     * 
     * @param state
     *            true se il bottone è premuto.
     */
    public void setPressed(final boolean state) {
        pressed = state;
        this.notifyEvent(new ButtonPressed(this));
    }

    /**
     * 
     * @return il pin del bottone.
     */
    public int getPinNum() {
        return pinNum;
    }

}
