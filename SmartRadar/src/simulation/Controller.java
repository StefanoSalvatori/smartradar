package simulation;

/**
 * Controller.
 *
 */
public class Controller {
    private EmulatedSmartRadar model;
    private ArduinoAgent arduino;
    private GUI gui;

    /**
     * Associa questo controller al sistema su raspberry e arduino
     * 
     * @param radar
     *            raspberry
     * @param proxSensor
     *            arduino
     */
    public void attachModel(final EmulatedSmartRadar radar, final ArduinoAgent proxSensor) {
        this.model = radar;
        this.arduino = proxSensor;
    }

    /**
     * Collega questo controller ad una grafica.
     * 
     * @param gui
     *            una gui
     */
    public void attachView(final GUI gui) {
        this.gui = gui;
    }

    /**
     * Il bottone 'on' è stato premuto sulla view.
     */
    public void onButtonPressed() {
        this.model.getOnButton().setPressed(true);
    }

    /**
     * Il bottone 'off' è stato premuto sulla view.
     */
    public void offButtonPressed() {
        this.model.getOffButton().setPressed(true);
    }

    /**
     * @return l'attuale angolo del sensore
     */
    public int getCurrentAngle() {
        return this.arduino.getAngle();
    }

    /**
     * @return la velocità di flash dei led
     */
    public int getLedFlashSpeed() {
        return model.flashSpeed();
    }

    /**
     * Comunica alla gui che l'angolo del sensore è cambiato
     * 
     * @param angle
     *            il nuovo angolo
     */
    public void notifyAngleChanged(final int angle) {
        if (this.gui != null) {
            this.gui.updateAngle(angle);
        }

    }

    /**
     * 
     * @return range sel sensore
     */
    public int getRange() {
        return this.model.getMaxDetectDist();
    }

    /**
     * Emula la presenza di un oggetto ad una certa distanza
     * 
     * @param distance
     *            la distanza a cui si trova l'oggetto
     */
    public void setDist(final int distance) {
        this.arduino.getProximitySensor().changeDistance(distance);
    }

    /**
     * Il radar notifica la gui che un oggetto è sgtato rilevato.
     */
    public void notifyDetected() {
        this.gui.blinkDetectedLed();
    }

    /**
     * Il radar notifica la gui se sta o meno tracciando un oggetto.
     * 
     * @param b
     *            true se sta tracciando
     * 
     */
    public void notifyTracking(final boolean b) {
        this.gui.setTracking(b);

    }

    /**
     * 
     * @return il raggio di visuale del sensore
     */
    public int getProxVisionAngle() {
        return this.arduino.getProximitySensor().getVisionAngle();
    }

    /**
     * Aumenta la velocità di rotazione del sensore
     */
    public void increaseSpeed() {
        this.arduino.setRotationDelay(this.arduino.getRotationDelay() - 10);
    }

    /**
     * Diminuisce la velocità di rotazione del sensore
     */
    public void decreaseSpeed() {
        this.arduino.setRotationDelay(this.arduino.getRotationDelay() + 10);
    }

}
