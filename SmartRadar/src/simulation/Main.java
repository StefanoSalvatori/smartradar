package simulation;

/**
 * Testing class. *
 */
public final class Main {
    private Main() {
    }

    /**
     * @param args
     *            args.
     */
    public static void main(final String[] args) {

        final EmulatedObservableButton onButton = new EmulatedObservableButton(17);
        final EmulatedObservableButton offButton = new EmulatedObservableButton(18);
        final EmulatedProximitySensor prox = new EmulatedProximitySensor(10, 11);
        final Controller controller = new Controller();
        final GUI view = new GUI(controller);
        final EmulatedSmartRadar radar = new EmulatedSmartRadar(onButton, offButton, controller);
        final ArduinoAgent arduino = new ArduinoAgent(prox, controller);
        radar.attachComunication(arduino);
        arduino.attachComunication(radar);
        controller.attachView(view);
        controller.attachModel(radar, arduino);
        view.setVisible(true);
        // Light led = new pse.modulo_lab_3_2.devices.dio_impl.Led(4);
        // Light led = new pse.modulo_lab_3_2.devices.p4j_impl.Led(4);
        // Button button = new pse.modulo_3_2.devices.p4j_impl.Button(17);
        radar.start();
        arduino.start();
    }
}
