package simulation;

import smartradar.common.ReactiveAgent;
import smartradar.common.event.Event;
import smartradar.common.event.Msg;
import smartradar.common.event.MsgEvent;
import smartradar.common.event.MsgType;

/**
 * Simula il comportamento del di Arduino
 *
 */
public class ArduinoAgent extends ReactiveAgent {

    private static final int MIN_DELAY = 10;
    private volatile int rotationDelay = 150;
    private final EmulatedProximitySensor prox;
    private volatile int angle;
    private ReactiveAgent rasp;
    private final Controller controller;

    private enum State {
        IDLE, SCANNING, STOP
    };

    private volatile State currentState;

    /**
     * 
     * @param prox
     *            un sensore di prossimità emulato
     * @param connected
     *            led 'connected'
     * @param controller
     *            controller per la comunicazione con la GUI
     */
    public ArduinoAgent(final EmulatedProximitySensor prox, final Controller controller) {
        this.prox = prox;
        this.currentState = State.IDLE;
        this.prox.addObserver(this);
        this.controller = controller;

    }

    @Override
    public void start() {
        super.start();
        this.angle = 90;
        this.controller.notifyAngleChanged(angle);
        /**
         * Simula la rotazione del sensore. Ogni volta che cambia angolo
         * aggiorna la grafica
         */
        new Thread(() -> {
            int i = -1;
            while (true) {
                try {
                    if (this.currentState == State.SCANNING) {
                        if (angle >= 180 || angle <= 0) {
                            i = -i;
                        }
                        angle += i;
                        controller.notifyAngleChanged(angle);
                        this.sendMsgTo(rasp, new Msg(this.prox.getObjDistance() + " " + this.angle, MsgType.DIST_RCV));
                    } else if (this.currentState == State.STOP) {
                        this.sendMsgTo(rasp, new Msg(this.prox.getObjDistance() + " " + this.angle, MsgType.DIST_RCV));
                    }
                    Thread.sleep(this.rotationDelay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "motion").start();

    }

    /**
     * Inizia la comunicazione con un altro {@link ReactiveAgent}
     * 
     * @param agent
     *            the agent to comunicate with
     */
    public void attachComunication(final ReactiveAgent agent) {
        this.rasp = agent;
    }

    /**
     * 
     * @return il sensore di prossimita
     */
    public EmulatedProximitySensor getProximitySensor() {
        return this.prox;
    }

    @Override
    protected void processEvent(final Event<?> ev) {
        switch (currentState) {
            case IDLE:
                if (ev instanceof MsgEvent) {
                    final MsgType msgType = ((MsgEvent) ev).getMsg().getType();
                    if (msgType.equals(MsgType.ON)) {
                        this.turnOn();
                    }
                }
                break;
            case SCANNING:
                if (ev instanceof MsgEvent) {
                    final MsgType msgType = ((MsgEvent) ev).getMsg().getType();
                    if (msgType.equals(MsgType.OFF)) {
                        this.turnOff();
                    } else if (msgType.equals(MsgType.TRACK)) {
                        this.currentState = State.STOP;
                    }
                }
                break;
            case STOP:
                if (ev instanceof MsgEvent) {
                    final MsgType msgType = ((MsgEvent) ev).getMsg().getType();
                    if (msgType.equals(MsgType.RESUME)) {
                        this.currentState = State.SCANNING;
                    } else if (msgType.equals(MsgType.OFF)) {
                        this.turnOff();
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * 
     * @return l'attuale angolo
     */
    public int getAngle() {
        return this.angle;
    }

    /**
     * 
     * @return la massima distanza di funzionamento del sensore
     */
    public int getProxRange() {
        return this.prox.getMaxDistance();
    }

    private void turnOn() {
        this.currentState = State.SCANNING;
    }

    private void turnOff() {
        this.currentState = State.IDLE;
    }

    public int getRotationDelay() {
        return this.rotationDelay;
    }

    public void setRotationDelay(final int newDelay) {
        this.rotationDelay = newDelay <= MIN_DELAY ? MIN_DELAY : newDelay;
    }
}
