package smartradar.devices.pi4j;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

import smartradar.devices.generic.Light;

/**
 * Led fisico su raspberry implementato tramite pi4j
 * 
 *
 */
public class PhysicalLed implements Light {
    private final int pinNum;
    private GpioPinDigitalOutput pin;

    /**
     * Creates a led.
     * 
     * @param pinNum
     *            pin where the led is connected
     */
    public PhysicalLed(final int pinNum) {
        this.pinNum = pinNum;
        try {
            final GpioController gpio = GpioFactory.getInstance();
            this.pin = gpio.provisionDigitalOutputPin(GpioConfig.PIN_MAP[pinNum]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void switchOn() {
        pin.high();
        // System.out.println("LIGHT ON - pin "+pin);
    }

    @Override
    public synchronized void switchOff() {
        pin.low();
        // System.out.println("LIGHT OFF - pin "+pin);
    }

    @Override
    public void flash(final int millis) {
        new Thread(() -> {
            try {
                this.switchOn();
                Thread.sleep(millis);
                this.switchOff();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public int getPin() {
        return this.pinNum;
    }

}
