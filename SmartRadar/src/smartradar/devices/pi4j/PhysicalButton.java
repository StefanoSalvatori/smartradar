package smartradar.devices.pi4j;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import smartradar.common.event.ButtonPressed;
import smartradar.common.event.ButtonReleased;
import smartradar.common.event.Event;
import smartradar.devices.generic.ObservableButton;

/**
 * Rappresenta un bottone fisico su raspberry implementato tramite la libreria
 * pi4j
 *
 */
public class PhysicalButton extends ObservableButton {

    private GpioPinDigitalInput pin;

    public PhysicalButton(final int pinNum) {
        super();
        try {
            final GpioController gpio = GpioFactory.getInstance();
            pin = gpio.provisionDigitalInputPin(GpioConfig.PIN_MAP[pinNum], PinPullResistance.PULL_DOWN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        pin.addListener(new ButtonListener(this));
    }

    @Override
    public synchronized boolean isPressed() {
        return pin.isHigh();
    }

    class ButtonListener implements GpioPinListenerDigital {
        private final PhysicalButton button;

        ButtonListener(final PhysicalButton button) {
            this.button = button;
        }

        public void handleGpioPinDigitalStateChangeEvent(final GpioPinDigitalStateChangeEvent event) {
            Event<?> ev = null;
            // System.out.println(" --> GPIO PIN STATE CHANGE: " +
            // event.getPin() + " = " + event.getState());
            if (event.getState().isHigh()) {
                ev = new ButtonPressed(button);
            } else {
                ev = new ButtonReleased(button);
            }
            notifyEvent(ev);
        }
    }
}
