package smartradar.devices.generic;

public interface Button {

    boolean isPressed();

}
