package smartradar.devices.generic;

public interface Light {

    void switchOn();

    void switchOff();

    void flash(int millis);
}
