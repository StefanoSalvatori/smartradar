package smartradar.devices.generic;

public interface ProximitySensor {

    /**
     * 
     * @return la distanza corrente rilevata. se non c'è nessuno oggetto
     *         dovrebbe ritornare la distanza massima del sensore oppure zero
     */
    int getObjDistance();

}
