package smartradar.comunication;

import smartradar.common.Observer;
import smartradar.common.ReactiveAgent;
import smartradar.common.event.Event;
import smartradar.common.event.Msg;
import smartradar.common.event.MsgAvailable;
import smartradar.common.event.MsgEvent;
import smartradar.common.event.MsgType;

/**
 * Classe singleton. "instrada" i messaggi da raspberry ad arduino e viceversa.
 */
public final class MsgHandler extends ReactiveAgent implements Observer {
    private static final String SPLITTER = " ";

    private final ObservableSerial serial;
    private final ReactiveAgent raspberry;
    private static MsgHandler instance;

    private MsgHandler(final ObservableSerial serial, final ReactiveAgent raspberry) {
        this.serial = serial;
        this.raspberry = raspberry;
        this.serial.addObserver(this);
    }

    public static MsgHandler getInstance() {
        if (instance != null) {
            return instance;
        } else {
            throw new IllegalStateException();
        }
    }

    public static void init(final ObservableSerial serial, final ReactiveAgent radar) {
        instance = new MsgHandler(serial, radar);
    }

    @Override
    protected void processEvent(final Event<?> ev) {
        if (ev.getSource().equals(this.raspberry) && ev instanceof MsgEvent) {
            final Msg msg = ((MsgEvent) ev).getMsg();
            this.serial.sendMsg(msg.getType().getCode());
            if (msg.getType().equals(MsgType.OFF)) {
                this.serial.close();
            }
        } else if (ev.getSource().equals(this.serial) && ev instanceof MsgAvailable) {
            final String[] msg = ((MsgAvailable) ev).getMsg().split(SPLITTER, 2);
            final MsgType code = MsgType.get(msg[0]);
            final String info = msg.length > 1 ? msg[1] : "";
            this.sendMsgTo(this.raspberry, new Msg(info, code));
        }
    }

    /**
     * Manda un messaggio di chiusura sia a raspberry che arduino, poi chiude la
     * seriale
     */
    public void stopSystem() {
        this.serial.sendMsg(MsgType.OFF.getCode());
        this.sendMsgTo(this.raspberry, new Msg("", MsgType.OFF));
        this.serial.close();
    }

}
