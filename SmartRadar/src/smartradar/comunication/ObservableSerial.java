package smartradar.comunication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.TooManyListenersException;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import smartradar.common.Observable;
import smartradar.common.event.MsgAvailable;

public class ObservableSerial extends Observable implements SerialPortEventListener {

    private SerialPort serialPort;
    private final BufferedReader input;
    private final OutputStream output;

    public ObservableSerial(final String port, final int rate) throws NoSuchPortException, PortInUseException,
            UnsupportedCommOperationException, IOException, TooManyListenersException {

        final CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(port);
        // open serial port, and use class name for the appName.
        final SerialPort serialPort = (SerialPort) portId.open(this.getClass().getName(), 2000);

        // set port parameters
        serialPort.setSerialPortParams(rate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

        // serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN);
        // open the streams
        input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
        output = serialPort.getOutputStream();

        // add event listeners
        serialPort.addEventListener(this);
        serialPort.notifyOnDataAvailable(true);

    }

    public void sendMsg(final String msg) {
        final char[] array = (msg + "\n").toCharArray();
        byte[] bytes = new byte[array.length];
        for (int i = 0; i < array.length; i++) {
            bytes[i] = (byte) array[i];
        }
        try {
            output.write(bytes);
            output.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This should be called when you stop using the port. This will prevent
     * port locking on platforms like Linux.
     */
    public synchronized void close() {
        if (serialPort != null) {
            serialPort.removeEventListener();
            serialPort.close();
        }
    }

    public synchronized void serialEvent(final SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                final String msg = input.readLine();
                this.notifyEvent(new MsgAvailable(msg, this));
            } catch (IOException e) {
                System.err.println("I/O err");
            }
        }
    }

}
