package smartradar.common;

/**
 * Basic controller.
 *
 */
public abstract class BasicController extends Thread {

    /**
     * 
     * @param ms
     *            tempo di wait.
     * @throws InterruptedException
     *             se il thread viene interrotto durante la wait.
     */
    protected void waitFor(final long ms) throws InterruptedException {
        Thread.sleep(ms);
    }
}
