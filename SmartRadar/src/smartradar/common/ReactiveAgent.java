package smartradar.common;

import smartradar.common.event.Msg;
import smartradar.common.event.MsgEvent;

public abstract class ReactiveAgent extends EventLoopController {

    protected ReactiveAgent(final int size) {
        super(size);
    }

    protected ReactiveAgent() {
        super();
    }

    protected boolean sendMsgTo(final ReactiveAgent agent, final Msg message) {
        final MsgEvent ev = new MsgEvent(message, this);
        return agent.notifyEvent(ev);
    }
}
