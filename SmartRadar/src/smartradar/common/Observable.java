package smartradar.common;

import java.util.LinkedList;
import java.util.List;

import smartradar.common.event.Event;

public class Observable {

    private final List<Observer> observers;

    protected Observable() {
        observers = new LinkedList<Observer>();
    }

   
    protected void notifyEvent(final Event<?> ev) {
        synchronized (observers) {
            for (final Observer obs : observers) {
                obs.notifyEvent(ev);
            }
        }
    }

   
    public void addObserver(final Observer obs) {
        synchronized (observers) {
            observers.add(obs);
        }
    }

    public void removeObserver(final Observer obs) {
        synchronized (observers) {
            observers.remove(obs);
        }
    }
}
