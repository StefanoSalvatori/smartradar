package smartradar.common;

import smartradar.common.event.Event;
import smartradar.common.event.MsgEvent;
import smartradar.common.event.MsgType;
import smartradar.devices.generic.ObservableButton;

public abstract class AbstractSmartRadar extends ReactiveAgent {
    protected static final int FLASH_SPEED = 100;
    protected ObservableButton onButton;
    protected ObservableButton offButton;

    protected int prevTrackingDist;
    protected int distRecv;
    protected int angleRecv;

    protected enum State {
        IDLE, CONNECTED, OBJECT_DETECTED, TRACKING
    };

    protected State currentState;
    protected int objCount;

    @Override
    protected void processEvent(final Event<?> ev) {
        switch (currentState) { // NOPMD
            case IDLE:
                if (ev.getSource().equals(this.onButton)) {
                    this.turnOn();
                }
                break;
            case CONNECTED:
                if (ev.getSource().equals(this.offButton)) {
                    this.turnOff();
                } else if (this.distReceivedEvent(ev)) {
                    final String received = ((MsgEvent) ev).getMsg().getString();
                    this.parseMsg(received);
                    if (this.distRecv < this.getMaxDetectDist()) {
                        this.objDetected();
                        if (this.distRecv < this.getTrackingDist()) {
                            this.startTracking();
                        } else {
                            this.switchState(State.OBJECT_DETECTED);
                        }
                    }
                    if (this.angleRecv == 0 || this.angleRecv == 180) {
                        this.scanComplete();
                    }
                }
                break;
            case OBJECT_DETECTED:
                if (ev.getSource().equals(this.offButton)) {
                    this.turnOff();
                } else if (this.distReceivedEvent(ev)) {
                    final String received = ((MsgEvent) ev).getMsg().getString();
                    this.parseMsg(received);
                    if (this.distRecv < this.getTrackingDist()) {
                        this.startTracking();
                    } else if (this.distRecv >= this.getMaxDetectDist()) {
                        this.switchState(State.CONNECTED);
                    }
                    if (this.angleRecv == 0 || this.angleRecv == 180) {
                        this.scanComplete();
                    }
                }
                break;
            case TRACKING:
                if (ev.getSource().equals(this.offButton)) {
                    this.turnOff();
                } else if (this.distReceivedEvent(ev)) {
                    final String received = ((MsgEvent) ev).getMsg().getString();
                    this.parseMsg(received);
                    if (this.distRecv != this.prevTrackingDist) {
                        this.trackDistChanged();
                        this.prevTrackingDist = this.distRecv;
                    }
                    if (this.distRecv > this.getTrackingDist()) {
                        this.stopTracking();
                    }
                }
                break;
            default:
                break;
        }
    }

    protected boolean distReceivedEvent(final Event<?> ev) {
        return ev instanceof MsgEvent && ((MsgEvent) ev).getMsg().getType().equals(MsgType.DIST_RCV);
    }

    protected void switchState(final State state) {
        this.currentState = state;
    }

    protected abstract int getMaxDetectDist();

    protected abstract int getTrackingDist();

    protected abstract void objDetected();

    protected abstract void scanComplete();

    protected abstract void turnOff();

    protected abstract void turnOn();

    protected abstract void stopTracking();

    protected abstract void startTracking();

    protected abstract void trackDistChanged();

    protected abstract void parseMsg(final String s);

}
