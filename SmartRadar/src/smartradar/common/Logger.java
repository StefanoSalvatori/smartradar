package smartradar.common;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

/**
 * Classe per gestire messaggi di log.
 *
 */

public final class Logger {

    private static final Logger SINGLETON = new Logger();
    private static final String FILE_NAME = "RADAR_log";
    private static final int LOG_WINDOW_SIZE = 500;
    private static final File LOG_FILE = new File("/home/pi/" + FILE_NAME);
    private static final Path FILE_PATH = Paths.get(LOG_FILE.getPath());

    private final JFrame logFrame = new JFrame("LOG");
    private final JTextArea logText = new JTextArea();
    private final JScrollPane scrollV = new JScrollPane(logText, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

    private Logger() {
        logFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent ev) {
                System.exit(-1);
            }
        });
        ((DefaultCaret) logText.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        logFrame.setLayout(new BorderLayout());
        logFrame.setSize(LOG_WINDOW_SIZE, LOG_WINDOW_SIZE);
        logFrame.add(scrollV);
        logFrame.setVisible(true);

    }

    public static Logger get() {
        return SINGLETON;
    }

    /**
     * Scrive un messaggio nel pannello di log e in stdouy.
     * 
     * @param message
     *            messaggio da scrivere
     */
    public void log(final String message) {
        System.out.println("  [" + Calendar.getInstance().getTime() + "] " + message);
        logText.setText(logText.getText() + "\n" + "  [" + Calendar.getInstance().getTime() + "] " + message);
    }

    /**
     * Scrive un messaggio nell'ultima riga del file "RADAR_log"
     * 
     * @param message
     *            messaggio da salvare
     */
    public void logFile(final String message) {
        try {
            if (!LOG_FILE.exists()) {
                LOG_FILE.createNewFile();
            }
            Files.write(FILE_PATH,
                    ("  [" + Calendar.getInstance().getTime() + "] " + message + System.getProperty("line.separator"))
                            .getBytes(),
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
