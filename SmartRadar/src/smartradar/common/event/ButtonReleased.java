package smartradar.common.event;

import smartradar.devices.generic.Button;

public class ButtonReleased implements Event<Button> {
    private final Button source;

    public ButtonReleased(final Button source) {
        this.source = source;
    }

    public Button getSource() {
        return source;
    }
}
