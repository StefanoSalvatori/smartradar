package smartradar.common.event;

/**
 * Interfaccia per un generico evento
 * 
 * @param <T>
 *            la classe sorgente dell'evento
 *
 */
public interface Event<T> {

    /**
     * @return la sorgente dell'evento
     */
    T getSource();

}
