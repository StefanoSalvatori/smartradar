package smartradar.common.event;

import smartradar.devices.generic.Button;

public class ButtonPressed implements Event<Button> {
    private final Button source;

    public ButtonPressed(final Button source) {
        this.source = source;
    }

    public Button getSource() {
        return source;
    }
}
