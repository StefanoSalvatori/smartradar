package smartradar.common.event;

import java.util.Arrays;

/**
 * Codici per i messaggi tra arduino e raspberry
 *
 */
public enum MsgType {

    /**
     * Tipi di messaggi generali.
     */
    OFF("OFF"), ON("ON"),

    /**
     * Da rasperry ad arduino.
     */
    TRACK("TRACK"), RESUME("SCAN"),

    /**
     * Da arduino a raspberry.
     */
    DIST_RCV("INFO");

    private final String code;

    /**
     * 
     * @param c
     *            stringa identificativa del tipo di messaggio
     */
    MsgType(final String c) {
        this.code = c;
    }

    /**
     * 
     * @return codifica del tipo di messaggio
     */
    public String getCode() {
        return this.code;
    }

    /**
     * 
     * @param c
     *            codice da ricercare
     * @return il tipo di messaggio che ha come codice la stringa c (null se
     *         questo non esiste)
     */
    public static MsgType get(final String c) {
        return Arrays
                .asList(MsgType.values())
                .stream()
                .filter(code -> code.getCode().equals(c))
                .findFirst()
                .orElse(null);
    }

}
