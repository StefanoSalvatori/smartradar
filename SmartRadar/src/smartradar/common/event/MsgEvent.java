package smartradar.common.event;

import smartradar.common.ReactiveAgent;

public class MsgEvent implements Event<ReactiveAgent> {

    private final Msg msg;
    private final ReactiveAgent from;

    public MsgEvent(final Msg msg, final ReactiveAgent from) {
        this.msg = msg;
        this.from = from;
    }

    public Msg getMsg() {
        return this.msg;
    }

    @Override
    public ReactiveAgent getSource() {
        return this.from;
    }
}
