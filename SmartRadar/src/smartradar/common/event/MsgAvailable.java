package smartradar.common.event;

import smartradar.comunication.ObservableSerial;

/**
 * Evento chiamato dalla seriale quando un messaggio è disponibile
 *
 */
public class MsgAvailable implements Event<ObservableSerial> {

    private final ObservableSerial source;
    private final String msg;

    /**
     * 
     * @param msg
     *            il messaggio nella seriale.
     * @param source
     *            la sorgente.
     */
    public MsgAvailable(final String msg, final ObservableSerial source) {
        this.source = source;
        this.msg = msg;
    }

    @Override
    public ObservableSerial getSource() {
        return this.source;
    }

    /**
     * 
     * @return il messaggio nella seriale.
     */
    public String getMsg() {
        return this.msg;
    }

}
