package smartradar.common.event;

/**
 * Definisce un generico messaggio.<br>
 * Un messaggio deve essere di un certo tipo {@link MsgType} e puo contenere una
 * stringa
 *
 */
public class Msg {

    private final String msgString;
    private final MsgType msgType;

    /**
     * Crea un messaggio vuoto di uno specifico tipo.
     * 
     * @param type
     *            tipo di messaggio
     */
    public Msg(final MsgType type) {
        this.msgType = type;
        this.msgString = "";
    }

    /**
     * Crea un messaggio che contiene anche una stringa
     * 
     * @param message
     *            stringa del messaggio
     * @param type
     *            tipo di messaggio
     */
    public Msg(final String message, final MsgType type) {
        this.msgString = message;
        this.msgType = type;
    }

    /**
     * 
     * @return la stringa associata al messaggio
     */
    public String getString() {
        return msgString;
    }

    /**
     * 
     * @return il tipo di messaggio
     */
    public MsgType getType() {
        return msgType;
    }

}
