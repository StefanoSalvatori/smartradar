package smartradar.common;

import smartradar.common.event.Msg;
import smartradar.common.event.MsgType;
import smartradar.comunication.MsgHandler;
import smartradar.devices.generic.Light;
import smartradar.devices.pi4j.PhysicalButton;
import smartradar.devices.pi4j.PhysicalLed;

/**
 * Smart Radar.
 */
public class SmartRadar extends AbstractSmartRadar {
    /**
     * Distanza a cui inzia la fase di tracking
     */
    protected static final int MIN_DIST = 10;
    protected static final int RADAR_RANGE = 30;

    private Light onLed;
    private Light detected;
    private Light tracking;

    private MsgHandler messagesHandler;

    /**
     * Costruttore.
     * 
     * @param onButton
     *            bottone per accendere il radar
     * @param offButton
     *            bottone per spegner il radar
     * @param onLed
     *            led che mostra se il radar è acceso
     * @param detected
     *            led che flasha quando un oggetto viene rilevato
     * @param tracking
     *            led che indica la fase di tracciamento
     * @param messagesHandler
     *            un agente che gestisce la comunicazione seriale con arduino
     */
    public void init(final PhysicalButton onButton, final PhysicalButton offButton, final PhysicalLed onLed,
            final PhysicalLed detected, final PhysicalLed tracking, final MsgHandler messagesHandler) {
        this.onLed = onLed;
        this.detected = detected;
        this.tracking = tracking;
        this.onButton = onButton;
        this.offButton = offButton;
        this.offButton.addObserver(this);
        this.onButton.addObserver(this);
        this.messagesHandler = messagesHandler;
        currentState = State.IDLE;
    }

    protected void objDetected() {
        this.objCount++;
        this.detected.flash(FLASH_SPEED);
        Logger.get().logFile("Object at dist:" + this.distRecv + " angle:" + this.angleRecv + "deg");
    }

    protected void scanComplete() {
        Logger.get().log("#objects found: " + this.objCount);
        this.objCount = 0;
    }

    protected void turnOff() {
        Logger.get().log("Radar Offline");
        onLed.switchOff();
        tracking.switchOff();
        detected.switchOff();
        this.sendMsgTo(this.messagesHandler, new Msg(MsgType.OFF));
        switchState(State.IDLE);
    }

    protected void turnOn() {
        Logger.get().log("Radar Online");
        onLed.switchOn();
        this.sendMsgTo(this.messagesHandler, new Msg(MsgType.ON));
        this.switchState(State.CONNECTED);

    }

    protected void stopTracking() {
        this.tracking.switchOff();
        this.sendMsgTo(this.messagesHandler, new Msg(MsgType.RESUME));
        Logger.get().log("Stop tracking");
        this.switchState(State.CONNECTED);
    }

    protected void startTracking() {
        this.prevTrackingDist = 0;
        this.tracking.switchOn();
        Logger.get().log("Tracking object dist: " + this.distRecv);
        this.sendMsgTo(this.messagesHandler, new Msg(MsgType.TRACK));
        this.switchState(State.TRACKING);
    }

    protected void parseMsg(final String received) {
        final String[] parsed = received.split(" ");
        this.distRecv = Integer.parseInt(parsed[0]);
        this.angleRecv = Integer.parseInt(parsed[1]);
    }

    @Override
    protected int getMaxDetectDist() {
        return RADAR_RANGE;
    }

    @Override
    protected int getTrackingDist() {
        return MIN_DIST;
    }

    @Override
    protected void trackDistChanged() {
        Logger.get().log("dist: " + this.distRecv);
    }
}
