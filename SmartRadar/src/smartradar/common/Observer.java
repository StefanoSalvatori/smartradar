package smartradar.common;

import smartradar.common.event.Event;

public interface Observer {

    
    boolean notifyEvent(final Event<?> ev);
}
