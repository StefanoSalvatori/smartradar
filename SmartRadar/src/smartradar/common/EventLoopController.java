package smartradar.common;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import smartradar.common.event.Event;

/**
 * Event Loop Controller.
 */
public abstract class EventLoopController extends Thread implements Observer {

    private static final int DEFAULT_EVENT_QUEUE_SIZE = 50;

    private final BlockingQueue<Event<?>> eventQueue;

    protected EventLoopController(final int size) {
        eventQueue = new ArrayBlockingQueue<Event<?>>(size);
    }

    protected EventLoopController() {
        this(DEFAULT_EVENT_QUEUE_SIZE);
    }

    protected abstract void processEvent(final Event<?> ev);

    public void run() {
        while (true) {
            try {
                final Event<?> ev = this.waitForNextEvent();
                this.processEvent(ev);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    protected void startObserving(final Observable object) {
        object.addObserver(this);
    }

    protected void stopObserving(final Observable object) {
        object.removeObserver(this);
    }

    protected Event<?> waitForNextEvent() throws InterruptedException {
        return eventQueue.take();
    }

    protected Event<?> pickNextEventIfAvail() throws InterruptedException {
        return eventQueue.poll();
    }

    public boolean notifyEvent(final Event<?> ev) {
        return eventQueue.offer(ev);
    }
}
