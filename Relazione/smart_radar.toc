\select@language {italian}
\select@language {italian}
\contentsline {section}{\numberline {1}Introduzione}{4}{section.1}
\contentsline {section}{\numberline {2}Analisi e Progettazione}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Specifiche}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Architettura a Task}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Comportamento del sistema e FSM}{4}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Centralina}{4}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Radar}{5}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Design dettagliato}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}C++}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Java}{5}{subsection.3.2}
\contentsline {section}{\numberline {4}Schema Fritzing}{6}{section.4}
